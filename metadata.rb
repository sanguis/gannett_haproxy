name 'gannett_haproxy'
maintainer 'Josh Beauregard'
maintainer_email 'josh.beaureg@gmail.com'
license 'all_rights'
description 'Installs/Configures gannett_haproxy'
long_description 'Installs/Configures gannett_haproxy'
version '0.1.0'

suggests 'yum', '~> 3.8.2'
