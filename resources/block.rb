#
# Author:: Josh Beauregard <josh.beaureg@gmail.com>
# Cookbook Name:: gannett_haproxy
# Resource:: block

#

default_action :create

actions :create, :delete

attribute :name, kind_of: String, name_attribute: true
attribute :type, kind_of: String
attribute :attributes, kind_of: Array
