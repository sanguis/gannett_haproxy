#
# Author:: Josh Beauregard <josh.beaureg@gmail.com>
# Cookbook Name:: gannett_haproxy
# Provider:: block
#

def whyrun_supported?
  true
end

use_inline_resources # if defined?(use_inline_resources)
def block_id
  "#{new_resource.type}-#{new_resource.name}"
end

action :create do
  node.normal['gannett_proxy']['blocks'][block_id] = {
    'type' => new_resource.type,
    'name' => new_resource.name,
    'attributes' => new_resource.attributes
  }
  Chef::Log.debug(node['gannett_proxy']['blocks'])
  template '/etc/haproxy/haproxy.cfg' do
    cookbook 'gannett_haproxy'
    source 'haproxy.cfg.erb'
    owner 'root'
    group 'root'
    mode '0644'
    variables(blocks: node['gannett_proxy']['blocks'])
    action :create
  end
  Chef::Log.info("created HAProxy #{block_id}.")
  @new_resource.updated_by_last_action(true)
end

action :delete do
  node['gannett_proxy']['blocks'].delete(block_id)
  template '/etc/haproxy/haproxy.cfg' do
    cookbook 'gannett_haproxy'
    source 'haproxy.cfg.erb'
    owner 'root'
    group 'root'
    mode '0644'
    action :create
  end
  Chef::Log.info("Deleted HAProxy #{@new_resource.type}, #{@new_resource.name}.")
  @new_resource.updated_by_last_action(true)
end
