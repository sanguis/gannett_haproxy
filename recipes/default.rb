#
# Cookbook Name:: gannett_haproxy
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.
#

package 'Install HAProxy' do
  package_name 'haproxy'
end

service 'haproxy' do
  supports status: true, restart: true, reload: true
  action :enable
end

template '/etc/haproxy/haproxy.cfg' do
  source 'haproxy.cfg.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables(blocks: nil)
  action :create_if_missing
  notifies :restart, 'service[haproxy]'
end
