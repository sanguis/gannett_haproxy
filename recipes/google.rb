#
# Cookbook Name:: gannett_haproxy
# Recipe:: google
#
# Copyright (c) 2015 The Authors, All Rights Reserved.
#

gannett_haproxy_block 'HAProxyLocalStats' do
  type 'listen'
  attributes [
    'bind 127.0.0.1:2200 name localstats',
    'mode http',
    'stats enable',
    'stats refresh 5',
    'stats admin if TRUE',
    'stats uri /'
  ]
  action :create
  notifies :restart, 'service[haproxy]'
end

gannett_haproxy_block 'goog-fr' do
  type 'frontend'
  attributes [
    'bind *:80 name 80',
    'mode http',
    'option  http-keep-alive',
    'default_backend google'
  ]
  action :create
  notifies :restart, 'service[haproxy]'
end

gannett_haproxy_block 'google' do
  type 'backend'
  attributes [
    'mode http',
    'option httpchk GET /',
    'http-request set-header Host www.google.com',
    'server google-srv google.com:443 ssl check inter 10000  verify none'
  ]
  action :create
  notifies :restart, 'service[haproxy]'
end
