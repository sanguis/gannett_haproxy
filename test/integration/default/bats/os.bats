#!/usr/bin/env bats


@test "CentOS is 6.7" {
    run cat /etc/redhat-release

      [ "$status" -eq 0 ]
      [ "$output" = "CentOS release 6.7 (Final)" ]
}
