#!/usr/bin/env bats


@test "HAProxy installed" {
    run which haproxy

      [ "$status" -eq 0 ]
}
