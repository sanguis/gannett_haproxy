require 'serverspec'

set :os, family: 'redhat', release: '6', arch: 'x86_64'
set :backend, :exec
